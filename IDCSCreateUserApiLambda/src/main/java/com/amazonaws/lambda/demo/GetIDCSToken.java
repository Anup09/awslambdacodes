package com.amazonaws.lambda.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;

public class GetIDCSToken {
	
	String tokenKey = "IDCS_TOKEN";
	String tokenExpirationSecsKey = "EXP_SECS";
	String tokenCreationTimeKey = "CREATION_TIME";
	String currentToken = new String();
	String clientID = "CLIENT_ID";
	String clientSecret = "CLIENT_SECRET";
	String host = "HOST"; 
	String secretName = "IDCS_TOKEN_SECRET";
	String token = null;
	AWSSecretsManager secretClient  = AWSSecretsManagerClientBuilder.standard().build();
	
	static final Logger logger = LogManager.getLogger(GetIDCSToken.class);
	
	public String getIDCSToken(){
		
	    String secret;

	    try {
	    	
	    logger.info("Retrieving token from Secret Manager");	
	    GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
	                    .withSecretId(secretName);
	    GetSecretValueResult getSecretValueResult = null;

	    
	        getSecretValueResult = secretClient.getSecretValue(getSecretValueRequest);
	        if (getSecretValueResult.getSecretString() != null) {
		        secret = getSecretValueResult.getSecretString();
		        
		        JSONObject json = new JSONObject(secret);
		        token = (String)json.get(tokenKey);
		        logger.info("Token retrieved from Secret Manager : "+token);	
	        }
		         
	        
	    }
	        catch (Exception e) {
		       logger.error(e.getMessage());
		    } 
	    return token;
	}
	
	
}


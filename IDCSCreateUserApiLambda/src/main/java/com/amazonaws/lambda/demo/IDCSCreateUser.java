package com.amazonaws.lambda.demo;

import java.io.BufferedReader;


import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.axis.encoding.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class IDCSCreateUser {
	
	static final Logger logger = LogManager.getLogger(IDCSCreateUser.class);
	//final static Logger logger = Logger.getLogger(LambdaFunctionHandler.class);
	
	public String createUser(Object requestBody, String idcs_token) {
		
		OutputStream os = null;
		InputStream inStream = null;
		InputStreamReader in = null;
		BufferedReader br = null;
		HttpURLConnection conn = null;
		String host = null;
		String authToken = null;
		String output = null;
		String responseObj = null;
		logger.info("Entering Create User Api Call");
		logger.info("Testing Devops pipeline 08/20");
		try{
			
			logger.info(requestBody);
			
			JSONObject requestJson = new JSONObject(requestBody.toString());
			logger.info("Auth token: "+idcs_token);
			
			
            host = System.getenv("ApiUrl");
            
            
            logger.info("HOST URL :"+host);
            
            
			URL url = new URL(host);
			//String requestXML = "grant_type=client_credentials&scope=urn:opc:idm:__myscopes__";
		    byte[] postData = requestJson.toString().getBytes( StandardCharsets.UTF_8 );
		    int postDataLength = postData.length;
			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("charset", "utf-8");
			conn.setRequestProperty("Content-Length", Integer.toString(postDataLength ));
			conn.setRequestProperty("Authorization", "Bearer " + idcs_token);
		    
			os = conn.getOutputStream();
			os.write(postData);
			os.flush();
			int responseCode = conn.getResponseCode();
			logger.info("Response Code: "+responseCode);
			
			if (responseCode > conn.HTTP_BAD_REQUEST || responseCode == conn.HTTP_BAD_REQUEST)
			{
				inStream = conn.getErrorStream();
				in = new InputStreamReader(inStream);
				br = new BufferedReader(in);
				while ((output = br.readLine()) != null) {
					
					responseObj = output;
					
				}
				
				logger.error(responseObj);
				
				
			}
			else {
				inStream = conn.getInputStream();
	            in = new InputStreamReader(inStream);
				br = new BufferedReader(in);
				while ((output = br.readLine()) != null) {
					
					responseObj = output;
					
				}
				logger.info("Response OBJ: "+responseObj);
			}

			
			
			

		} catch (MalformedURLException e) {

			logger.error("Malformed URL Exception occured while REST WS call: " + e.getMessage());

		} catch (UnknownHostException e) {
			logger.error("UnknownHost Exception occured while REST WS call: " + e.getMessage());
		}

		catch (IOException e) {

			logger.error("IOException occured while REST WS call: " + e.getMessage());

		}

		catch (Exception e) {
			logger.error("Exception occured while REST WS call: "+ e.getMessage());

		} finally {

			try {
				if (conn != null) {
					logger.info("Closing Connection");
					conn.disconnect();
				}
			
				if (os != null) {
					os.close();
				}
				if (inStream != null) {
					inStream.close();
				}
				if (br != null) {
					br.close();
				}

			} catch (IOException e) {
				logger.error("IOException occured: " + e.getMessage());

			}
		}

		return responseObj;

	}

}




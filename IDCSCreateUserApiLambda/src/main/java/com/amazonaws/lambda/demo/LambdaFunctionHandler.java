package com.amazonaws.lambda.demo;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.*;

public class LambdaFunctionHandler implements RequestHandler<Object, String> {

    @Override
    public String handleRequest(Object input, Context context) {
        
    	String logFilePath = System.getenv("logFilePath");
        
	    System.setProperty("log4j.configurationFile", logFilePath);
        //Get IDCS token form AWS secret manager
    	GetIDCSToken getIdcsToken = new GetIDCSToken();
		String token = getIdcsToken.getIDCSToken();
		
		//Method call -  CreateUser API
		
		IDCSCreateUser idcsCreateUser = new IDCSCreateUser();
		String apiResponse = idcsCreateUser.createUser(input, token);

        return apiResponse;
    }

}
